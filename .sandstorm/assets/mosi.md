## Mosi
### a tiny game engine

"A tiny game engine that lets you make tiny game worlds." - from the official GitHub repo

Documentation available at [wiki](https://github.com/zenzoa/mosi/wiki)

**PLEASE NOTE:** This is a "client-only" app. That means that any changes you make are not saved in the grain, but rather your browser's local storage. Please keep this in mind when using the app.
